package DAO

import Models.Intents
import Models.Problem
import Models.User
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.SQLException

object DatabaseFactory {
    fun init(){
        try {
            val usuario = "reino"
            val contrasena = "reino"
            val driverClassName = "org.postgresql.Driver"
            val jdbcUrl = "jdbc:postgresql://localhost:5432/jutge"
            val conection = Database.connect("jdbc:postgresql://localhost:5432/jutge",driverClassName,"reino",contrasena)

            transaction(conection){
                SchemaUtils.create(Problem,User,Intents)
            }



        } catch (e: SQLException) {
            println("Error: " + e.errorCode + e.message)
        }
    }
    suspend fun <T> dbQuery(block: suspend () -> T): T {
        return newSuspendedTransaction(Dispatchers.IO) {
            block()
        }
    }

}