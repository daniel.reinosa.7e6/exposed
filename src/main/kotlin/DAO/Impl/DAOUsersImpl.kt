package DAO.Impl

import DAO.DAOIntents
import DAO.DAOUsers
import DAO.DatabaseFactory
import DAO.DatabaseFactory.dbQuery
import Models.Intents
import Models.Problem
import Models.User
import Models.dcUser
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOUsersImpl:DAOUsers {
    private fun resultRowToUserList(row: ResultRow) = dcUser(
        userId = row[User.userId],
        name = row[User.name],
        role = row[User.role],
    )


    override suspend fun getAllUsers(): List<dcUser> = dbQuery {
        User.selectAll().map(::resultRowToUserList)
    }


    override suspend fun getUserByID(id: Int): dcUser? = dbQuery {
        User
            .select { Intents.userId eq id }
            .map(::resultRowToUserList)
            .singleOrNull()
    }
    override suspend fun addNewUser(userId: Int?, name: String, role:String): dcUser? = dbQuery {
        val insertStatement = User.insert {
            it[User.name] = name
            it[User.role] = role
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToUserList)
    }
    override suspend fun editUser(userId: Int, name: String, role:String):Boolean = dbQuery{
        User.update({ Intents.userId eq Problem.id }) {
            it[User.userId] = userId
            it[User.name] = name
            it[User.role] = role
        } > 0
    }
    override suspend fun deleteUser(id: Int):Boolean = dbQuery {
        User.deleteWhere { Intents.userId eq id } > 0
    }





}
/*


val dao: DAOUsers = DAOUsersImpl().apply {
    runBlocking {
        if (getAllUsers().isEmpty()) {
            addNewUser(0, "dani", "Alumno")
        }
    }
}
 */