package DAO.Impl

import DAO.DAOIntents
import DAO.DatabaseFactory
import DAO.DatabaseFactory.dbQuery
import Models.*
import Models.Problem.id
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOIntentsImpl: DAOIntents {
    private fun resultRowToIntentsList(row: ResultRow) = dcIntents(
        userId = row[Intents.userId],
        intents = row[Intents.intents],
        numProblema = row[Intents.numProblema],
        resuelto = row[Intents.resuelto],
    )
    override suspend fun getAllIntents(): List<dcIntents> = DatabaseFactory.dbQuery {
        TODO("Not yet implemented")
        Intents.selectAll().map(::resultRowToIntentsList)

    }
override suspend fun UserIntents(id: Int): dcIntents? = dbQuery{
    Intents
        .select { Intents.userId eq id }
        .map(::resultRowToIntentsList)
        .singleOrNull()

}
     override suspend fun addNewIntent(userId: Int, intents: Int, numProblema:Int, resolt:Boolean): dcIntents?= dbQuery {

        val insertStatement = Intents.insert {
            it[Intents.userId] = userId
            it[Intents.numProblema] = numProblema
            it[Intents.intents] = intents
            it[Intents.resuelto] = resolt

        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToIntentsList)

    }
    override suspend fun editIntent(userId: Int, intents: Int, numProblema:Int, resolt:Boolean): Boolean = dbQuery {
        Intents.update({ Intents.userId eq id }) {
            it[Intents.userId] = userId
            it[Intents.intents] = intents
            it[Intents.numProblema] = numProblema
            it[Intents.resuelto] = resolt
        } > 0

    }
    override suspend fun deleteIntent(id: Int):Boolean = dbQuery {
        Intents.deleteWhere { Intents.userId eq id } > 0
    }
//    val dao: DAOIntents = DAOIntentsImpl().apply {
//        runBlocking {
//            if (getAllIntents().isEmpty()) {
//                addNewIntent(0, 4, 6, true)
//            }
//        }
//    }

}