package DAO.Impl

import DAO.DAOProblem
import DAO.DatabaseFactory.dbQuery
import Models.*
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOProblemImpl: DAOProblem {

    private fun resultRowToProblemList(row: ResultRow) = dcProblem(
        idProblem = row[Problem.id],
        title = row[Problem.title],
        statement = row[Problem.statement],
        entrance = row[Problem.entrance],
        publicInput = row[Problem.publicInput],
        publicOutput = row[Problem.publicOutput],
        privateInput = row[Problem.privateInput],
        privateOutput = row[Problem.privateOutput],
    )
    override suspend fun getAllProblems(): List<dcProblem> = dbQuery {
        Problem.selectAll().map(::resultRowToProblemList)
    }



    override suspend fun getProblem(id: Int): dcProblem? = dbQuery {
        Problem
            .select { Problem.id eq id }
            .map(::resultRowToProblemList)
            .singleOrNull()
    }
    override suspend fun addNewProblem(title:String,statement: String,entrance: String,publicInput:String,publicOutput:String,privateInput:String,privateOutput:String)
    : dcProblem? = dbQuery {
        val insertStatement = Problem.insert {
            it[Problem.title] = title
            it[Problem.statement] = statement
            it[Problem.entrance] = entrance
            it[Problem.publicInput] = publicInput
            it[Problem.publicOutput] = publicOutput
            it[Problem.privateInput] = privateInput
            it[Problem.privateOutput] = privateOutput
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToProblemList)

    }
    override suspend fun editProblem(idProblem:Int,title:String,statement: String,entrance: String,publicInput:String,publicOutput:String,privateInput:String,privateOutput:String):Boolean = dbQuery{
        User.update({ Intents.userId eq Problem.id }) {
            it[Problem.id] = Problem.id
            it[Problem.title] = title
            it[Problem.statement] = statement
            it[Problem.entrance] = entrance
            it[Problem.publicInput] = publicInput
            it[Problem.publicOutput] = publicOutput
            it[Problem.privateInput] = privateInput
            it[Problem.privateOutput] = privateOutput
        } > 0
    }
    override suspend fun deleteProblem(id: Int):Boolean = dbQuery {
        User.deleteWhere { Intents.userId eq id } > 0
    }
//    val dao: DAOProblem = DAOProblemImpl().apply {
//        runBlocking {
//            if (getAllProblems().isEmpty()) {
//           //     addNewProblem(0, 4, 6, true)
//            }
//        }
//    }


}