package DAO

import Models.dcIntents
import Models.dcProblem

interface DAOProblem {
    suspend fun getAllProblems():List<dcProblem>
    suspend fun getProblem(id: Int): dcProblem?
    suspend fun addNewProblem(
        title:String,
        statement: String,
        entrance: String,
        publicInput:String,
        publicOutput:String,
        privateInput:String,
        privateOutput:String): dcProblem?
    suspend fun editProblem(idProblem:Int,title:String,statement: String,entrance: String,publicInput:String,publicOutput:String,privateInput:String,privateOutput:String):Boolean
    suspend fun deleteProblem(id: Int):Boolean


}