package DAO

import Models.dcUser

interface DAOUsers {
    suspend fun getAllUsers(): List<dcUser>
    suspend fun getUserByID(id: Int): dcUser?
    suspend fun addNewUser(userId: Int?, name: String, role:String): dcUser?
    suspend fun editUser(userId: Int, name: String, role:String):Boolean
    suspend fun deleteUser(id: Int):Boolean
}