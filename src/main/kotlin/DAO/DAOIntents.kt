package DAO

import Models.dcIntents
import Models.dcUser

interface DAOIntents {
    suspend fun getAllIntents(): List<dcIntents>
    suspend fun UserIntents(id: Int): dcIntents?
    suspend fun addNewIntent(userId: Int, intents: Int, numProblema:Int, resolt:Boolean):dcIntents?
    suspend fun editIntent(userId: Int, intents: Int, numProblema:Int, resolt:Boolean):Boolean
    suspend fun deleteIntent(id: Int):Boolean
}