package Models

import DAO.Impl.DAOIntentsImpl
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.Table
import java.lang.Exception
import java.util.*

const val colorGris = "\u001b[37m"
const val colorGroc = "\u001b[33m"
const val colorVerd = "\u001b[32m"
const val colorRed = "\u001b[31m"
const val colorReset = "\u001b[0m"
const val colorBlue = "\u001b[34m"
val scan = Scanner(System.`in`)

@Serializable
data class dcProblem(
    val idProblem:Int,
    val title:String,
    val statement: String,
    val entrance: String,
    val publicInput:String,
    val publicOutput:String,
    val privateInput:String,
    val privateOutput:String
){
     var userPoints = 100
     var solved = false
     var trys = mutableListOf<String>()
     var intents = 0

     fun printProblem(n:Int){
         println("${colorGroc}Programa ${n+1}$colorReset: ${this.title}")
         println()
         println()
         println(this.statement)
         println(this.entrance)
         println("${colorVerd}Exemple:$colorReset")
         println("     ${colorGroc}Input:$colorReset ${this.publicInput}")
         println("     ${colorGroc}Output:$colorReset ${this.publicOutput}")
         println()
         println("     ${colorGroc}Input:$colorReset ${this.privateInput}")
         println("     ${colorGris}Output:$colorReset ¿?")
         println()
     }

     suspend fun resolvedProblem(userID:Int){
         do{
             intents++
             print("Escribe la respuesta:")
             val solution = scan.next()

             if(solution != this.privateOutput) {
                 println("Es incorrecto")
                 this.stats()
                 userPoints -= 10
                 this.userPoints

             }
             else{
                 println()
                 println("Es correcto")
                 this.solved = true
                 this.trys.add(solution)
                 this.stats()

             }
             addIntent(userID)


         }while (!this.solved)
     }
    suspend fun addIntent(userId:Int){
        try {
            DAOIntentsImpl().addNewIntent(userId,intents,idProblem,solved)
        } catch (e: ExposedSQLException) {
            println(e)
        } catch (e: Exception) {
            println(e)
        }


    }
     fun stats(){
         println()
         println("Tus intentos: $intents")
         for(i in this.trys.indices) println("   - ${this.trys[i]}")
         if(!this.solved)println("${colorRed}no resuelto$colorReset")
         else  println("${colorVerd}resuelto$colorReset ")
         println()
     }


 }
object Problem: Table() {
    val id = integer("idProblem").autoIncrement()
    val title = varchar("title",200)
    val statement = varchar("statement",1024)
    val entrance = varchar("entrance",1024)
    val publicInput = varchar("publicInput",100)
    val publicOutput = varchar("publicOutput",100)
    val privateInput = varchar("privateIntput",100)
    val privateOutput = varchar("privateOutput",100)

    override val primaryKey = PrimaryKey(id)
}

