package Models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
data class dcUser(
    val userId: Int?,
    val name:String,
    val role:String
)
object User: Table(){
    val userId = integer("userId").autoIncrement()
    val name = varchar("name",200)
    val role = varchar("role", 200)

    override val primaryKey = PrimaryKey(User.userId)

}