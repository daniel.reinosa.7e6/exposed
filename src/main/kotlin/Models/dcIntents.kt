package Models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table


@Serializable
data class dcIntents(
    val userId: Int,
    val intents: Int,
    val numProblema: Int,
    val resuelto: Boolean
)
object Intents: Table(){
    val intents = integer("intents")
    val userId = integer("userId")
    val numProblema = integer("numProblema")
    val resuelto = bool("resuelto")


}