import DAO.DatabaseFactory
import DAO.Impl.DAOIntentsImpl
import DAO.Impl.DAOProblemImpl
import DAO.Impl.DAOUsersImpl
import Models.Problem
import Models.dcIntents
import Models.dcProblem
import Models.dcUser
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.statements.BatchInsertStatement
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import java.lang.Exception
import java.util.*
import kotlin.io.path.Path

val scan = Scanner(System.`in`)
const val colorGris = "\u001b[37m"
const val colorGroc = "\u001b[33m"
const val colorVerd = "\u001b[32m"
const val colorRed = "\u001b[31m"
const val colorReset = "\u001b[0m"
const val colorBlue = "\u001b[34m"



suspend fun main() {
    DatabaseFactory.init()
    println()
    println()
    println("\n" +
            "                  ░░░░░██╗██╗░░░██╗████████╗░██████╗░███████╗\n" +
            "                  ░░░░░██║██║░░░██║╚══██╔══╝██╔════╝░██╔════╝\n" +
            "                  ░░░░░██║██║░░░██║░░░██║░░░██║░░██╗░█████╗░░\n" +
            "                  ██╗░░██║██║░░░██║░░░██║░░░██║░░╚██╗██╔══╝░░\n" +
            "                  ╚█████╔╝╚██████╔╝░░░██║░░░╚██████╔╝███████╗\n" +
            "                  ░╚════╝░░╚═════╝░░░░╚═╝░░░░╚═════╝░╚══════╝")
    println()
    println()
    userRegister()
}

 suspend fun userRegister(){
    do {
        println("Indica tu Nombre")
        val userName = scan.next().uppercase()
        println("Indica tu rol (Alumno/Profe)")
        val userRole = scan.next().uppercase()
        var userID:Int = 0


        runBlocking {
            withContext(coroutineContext){
                DAOUsersImpl().addNewUser(null,userName,userRole)
                val idList = DAOUsersImpl().getAllUsers()
                userID = idList.lastIndex
            }

        }


        if (getProblems().isEmpty()){
            importDatabase()
            println("La base de datos ha sido actualizada :)")
        }
        menu(userRole,userID)
    }while (userRole != "ALUMNO" || userRole != "PROFE")

}




suspend fun menu(rol:String,userId: Int){
    if (rol == "ALUMNO"){
        do{

        println("${colorBlue}1 -Jutge\n" +
                "2 -Seguir con itenerario de aprendizaje\n" +
                "3 -Lista de problemas\n" +
                "4 -Consultar historico de problemas resueltos\n" +
                "5 -Ayuda\n" +
                "6 -Salir\n$colorReset")
        val sc = scan.nextInt()
        when(sc){
            1 ->jutge(userId)
            2 ->seguirItinerari()
            3 ->problemList()
            4 ->resolvedProblems()
            5 ->help()
            6 ->exit()
        }}
        while (sc != 6)
    }else if (rol == "PROFE"){

        do {
            println("${colorBlue}1 -Añadir nuevos problemas\n" +
                    "2 -Sacar reporte de la faena del Alumno \n" +
                    "3 -Salir$colorReset")
            val sc = scan.nextInt()
            when(sc){
                1 -> addProblem()
                2 -> report()
                3 -> mostrar()
                4 -> exit()
            }

        }while (sc != 3)
    }
}
suspend fun mostrar(){
    var getAllIntents = listOf<dcIntents>()
    withContext(Dispatchers.IO) {
        newSuspendedTransaction {

            try {
                getAllIntents = DAOIntentsImpl().getAllIntents()
            } catch (e: ExposedSQLException) {
                println(e)
            } catch (e: Exception) {
                println(e)
            }
        }
    }
    println(getAllIntents)
}
//Funcions Llista Alumne
suspend fun jutge(userId: Int) {
    val problemListInDatabase = getProblems()
    var gamePosition = 0
    var resolvedQuestions = 0
    var finish = true
    do {
        for (i in problemListInDatabase.indices) {
            val problemPosition = problemListInDatabase[i]
            problemPosition.printProblem(i)
            println(
                "     ${colorBlue}Enserio quieres resolverlo?$colorReset\n" +
                        "${colorVerd}Y$colorReset/${colorRed}N$colorReset\n"
            )
            val userDecide = scan.next().uppercase()
            if (userDecide == "Y") {
                resolvedQuestions++
                gamePosition++
                problemPosition.resolvedProblem(userId)
            } else if (userDecide == "N") {
                finish = false
                val text = ""
                println(text)
            }
        }
    } while (finish)

}

fun seguirItinerari(){
    var problemNum = 1
    val resolvedProblem = true
    var printisResolv = ""
    var mutableListOfProblems = mutableListOf<Boolean>()
    for (i in problemslist.indices){
     problemNum ++
    if(resolvedProblem == true){
        printisResolv = "Resuelto"
    }else printisResolv = "No Resuelto"
   // mutableListOfProblems.add(problemslist[i].isResolved)

    println("Numero de problema $problemNum, ${mutableListOfProblems[i]}")
    }



}
suspend fun resolvedProblems(){
    try {
        DAOIntentsImpl().getAllIntents().listIterator()
    }catch (e: Exception){
        println(e)
    }
}

suspend fun importDatabase(){
    try {
        for (problem in problemslist){
            DAOProblemImpl().addNewProblem(problem.title,problem.statement,problem.entrance,problem.publicInput,problem.publicOutput,problem.privateInput,problem.privateOutput)
        }
    }catch (e: Exception){
        println(e)
    }
}
suspend fun getProblems():List<dcProblem>{
    var allProblems = listOf<dcProblem>()
    try {
        allProblems = DAOProblemImpl().getAllProblems()
    }catch (e: Exception){
        println(e)
    }
    return allProblems
}
suspend fun problemList(){
    //L’opció de llista de problemes mostra una llista amb tots els problemes que existeixen al sistema i en permet seleccionar-ne un.

    val problemList = getProblems()
    for (problem in problemList){
        println("${problem.idProblem}-${problem.title}" )
    }

    println("Indica el numero de problema que quieres resolver")
    val userNum = scan.nextInt()
    for (problem in problemList){
       if (problem.idProblem == userNum){
           println()
       }
    }
}
fun help(){
    println("\n" +
        "           ─█▀▀█ ░█──░█ ░█─░█ ░█▀▀▄ ─█▀▀█ 　 ── 　 ───░█ ░█─░█ ▀▀█▀▀ ░█▀▀█ ░█▀▀▀ 　 ▀█▀ ▀▀█▀▀ ░█▀▀█ \n" +
        "           ░█▄▄█ ░█▄▄▄█ ░█─░█ ░█─░█ ░█▄▄█ 　 ▀▀ 　 ─▄─░█ ░█─░█ ─░█── ░█─▄▄ ░█▀▀▀ 　 ░█─ ─░█── ░█▀▀▄ \n" +
        "           ░█─░█ ──░█── ─▀▄▄▀ ░█▄▄▀ ░█─░█ 　 ── 　 ░█▄▄█ ─▀▄▄▀ ─░█── ░█▄▄█ ░█▄▄▄ 　 ▄█▄ ─░█── ░█▄▄█")
    println()
    println()
    println("           El juego consiste en resolver las preguntas")
    println("           Solo hay dos tipos de usuario para este juego, uno es Alumno el cual puede jugar y ver el menu de opciones.\n" +
            "           El otro es Profesor, el cual puede revisar el numero de intentos de cada usuario y tambien puede añadir nuevos problemas")
    println()
    println()
    println()
    println("${colorGroc}-Pulsa cualquier tecla para salir$colorReset")
    val sc = scan.next().uppercase()
}
fun exit(){
    kotlin.system.exitProcess(0)
}
// Funcions Professorat
suspend fun addProblem() {
    val listOfProblemstoDatabase = Path("")

    println("Indica el titulo del problema.")
    val profProblem = scan.next()
    println("Indica el enunciado del problema.")
    val profStatment = scan.next()
    println("Indica un contenido (Opcional)")
    val profContent = scan.next()
    println("Indica un valor")
    val profPublicInput = scan.next()
    println("Indica la respuesta de este valor")
    val profPublicOutput = scan.next()
    println("Indica otro valor")
    val profPrivateInput = scan.next()
    println("Indica la respuesta de este valor (este no se mostrará)")
    val profPrivateOutput = scan.next()
    try {
        DAOProblemImpl().addNewProblem(profProblem,profStatment,profContent,profPublicInput,profPublicOutput,profPrivateInput,profPrivateOutput)

    }catch (e: Exception){
        println(e)
    }
}


fun report(){
    try {
      //  DAOIntentsImpl().getAllIntents()
    }catch (e: Exception){
        println(e)
    }
}

/*
fun itinerariAprenentatgeDB( listaProblemas: List<Problem>, listaIntentos: List<Intent>): MutableList<Int> {
    val listaNumeros = mutableListOf<Int>()
    for (problema in listaProblemas){
        listaNumeros.add(problema.numProblema)
    }
    for (intento in listaIntentos) {
        if (intento.resuelto){
            listaNumeros.remove(intento.numProblema)
        }
    }
    return listaNumeros
}*/
