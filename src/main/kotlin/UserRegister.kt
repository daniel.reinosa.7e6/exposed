import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class UserRegister(
    val name:String,
    val role:String,
    ){
    fun userLog(name: String,role: String){
        val logFile = File("Jutge/src/main/resources/userLog.Json")
        val log = mutableListOf<UserRegister>()
        log.add(UserRegister(name,role))
        val toJson = Json.encodeToString(log)
        logFile.appendText("\n$toJson")
    }

}